#!/usr/bin/python3

from enum import Enum
import threading
import socket
import struct
import uuid
import sys
import os
from datetime import datetime

#  OPCodes
import logging
OP__ = b'\x00'  # Space
OP_RPQ = b'\x00\x01'  # Read request
OP_WRQ = b'\x00\x02'  # Write request
OP_DATA = b'\x00\x03'  # Data
OP_ACK = b'\x00\x04'  # Accepted
OP_Err = b'\x00\x05'   # Error
OP_OACK = b'\x00\x06' # Options Acceptance


def create_message(list_of_bytes):
    return b''.join(list_of_bytes)


def intersperse(lst, item):
    result = [item] * (len(lst) * 2 - 1)
    result[0::2] = lst
    return result


MAX_BLOCK_ID = 2**16 - 1
MAXIMAL_ACCEPTANCE_BLOCKSIZE = 2**13 - 1


def next_block_id(block_id):
    return (block_id + 1) % (MAX_BLOCK_ID + 1)


class TFTP(object):
    def __init__(self, host="127.0.0.1", port=69):
        self.host = host
        self.server_port = port
        self.mode = b"octet"
        self.uuid = uuid.uuid1()
        self.last_message = b''
        self.MAX_NUMBER_OF_CONNECTIONS = 1
        self.MAX_DATA_LENGTH = 512
        self.MAX_PACKET_LENGTH = 4 + self.MAX_DATA_LENGTH

    def send_block_accept(self, block_id, sock=None, host=None):
        query = [OP_ACK,
                 struct.pack('>H', block_id)]
        if host is None:
            host = (self.host, self.server_port)
        if sock is None:
            sock = self.sock
        self.send(query=query, remote=host, sock=sock)

    def send_data_block(self, data, block_id, sock=None, host=None):
        query = [OP_DATA,
                 struct.pack('>H', block_id),
                 data]
        if host is None:
            host = (self.host, self.server_port)
        if sock is None:
            sock = self.sock
        self.send(query=query, remote=host, sock=sock)

    def send(self, query, remote=None, sock=None):
        msg = create_message(query)
        if sock is None:
            sock = self.sock
        if remote is None:
            remote = (self.host, self.server_port)
        logging.info("{} : Sending message {} to {}".format(self.uuid, msg, remote))
        sock.sendto(msg, remote)
        self.last_message = msg

    def run_forever(self):
        raise NotImplementedError

    def exit(self, *args, **kwargs):
        raise NotImplementedError

    def __enter__(self):
        raise NotImplementedError

    def __exit__(self, exc_type, exc_val, exc_tb):
        raise NotImplementedError


def init_connection_vars(args):
    global host, port, path

    host = "localhost"
    port = 69
    path = 'server_storage'

    l = len(args)
    if l > 1:
        port = int(args[1])
    if l > 2:
        path = args[2]


class ConnectionStatus(Enum):
    CREATED = 1
    OPTIONS_NEGOTIATION = 2
    RECEIVING = 3
    TRANSMITTING = 4
    DONE = 5


class Connection(threading.Thread):
    def __init__(self, sock, client_port, root, data):
        super(Connection, self).__init__()
        self.uuid = uuid.uuid1()
        self.sock = sock
        self.sock.settimeout(0.5)
        self.status = ConnectionStatus.CREATED
        self.filename = ''
        self.client_port = client_port
        self.root = root
        self.block_id = 1
        self.file = None
        self.last_chunk = None
        self.blocks = dict()
        self.data = data
        self.BLOCK_SIZE = self.root.MAX_DATA_LENGTH
        self.WINDOW_SIZE = 1
        self.accepted_options = dict()
        self.last_ack = -1
        self.current_ack = 0
        self.creation_time = datetime.now()

    def transmitt_next_chunk(self):
        self.status = ConnectionStatus.TRANSMITTING
        # Last data block
        if self.block_id in self.blocks and len(self.blocks[self.block_id]) < self.BLOCK_SIZE:
            self.status = ConnectionStatus.DONE
            return
        # Increment starting block
        self.block_id = next_block_id(self.current_ack)

        # Remove old blocks
        current_blocks = self.blocks.keys()
        for buff_block_id in current_blocks:
            if buff_block_id <= self.current_ack:
                del self.blocks[buff_block_id]

        # Send WINDOW_SIZE chunks to client
        for k in range(0, self.WINDOW_SIZE):
            # If not have chunk in stored blocks -> read it!
            if self.block_id + k not in self.blocks:
                self.blocks[self.block_id + k] = self.file.read(self.BLOCK_SIZE)
            # Transmitt next chunk
            self.root.send_data_block(data=self.blocks[self.block_id + k],
                                      block_id=self.block_id + k,
                                      host=self.client_port,
                                      sock=self.sock)
            if self.block_id + k in self.blocks and len(self.blocks[self.block_id + k]) < self.BLOCK_SIZE:
                return

    def run(self):
        while 1:
            if self.status == ConnectionStatus.DONE:
                self.sock.close()
                self.root.drop_connection(self.client_port)
                #self.root.end = True
                break
            command, parts = self.data[:2], self.data[2:].split(OP__)
            try:
                #  ------------ Reading Request ------------
                if command == OP_RPQ:
                    # We receive some unsual stuff
                    if self.status != ConnectionStatus.CREATED:
                        return
                    # Reading Request
                    self.filename = parts[0].decode('utf-8')
                    if not os.path.isfile(self.filename):
                        raise ValueError
                    # Options
                    if len(parts) > 2:
                        parts[2:] = [x.decode('utf-8') for x in parts[2:]]
                        options = dict(zip(parts[2:-1:2], parts[3::2]))
                        if 'blksize' in options:
                            self.BLOCK_SIZE = min(int(options['blksize']), MAXIMAL_ACCEPTANCE_BLOCKSIZE)
                            self.accepted_options['blksize'] = self.BLOCK_SIZE
                        if 'windowsize' in options:
                            self.WINDOW_SIZE = max(1, int(options['windowsize']))
                            self.accepted_options['windowsize'] = self.WINDOW_SIZE
                    # Open file descriptor for reading
                    try:
                        self.file = open(self.filename, "rb")
                        if len(self.accepted_options) > 0:
                            self.root.options_acceptance(options=self.accepted_options,
                                                         host=self.client_port,
                                                         sock=self.sock)
                            self.block_id = 1
                        else:
                            self.transmitt_next_chunk()
                    except Exception as e:
                        logging.error("{} : Error during file transmitting. {}".format(self.root.uuid, e))
                # ------------- Block Acceptance --------------
                elif command == OP_ACK:
                    # Accepted
                    self.current_ack = struct.unpack(">H", self.data[2:])[0]

                    # Send or not send next chunk (maybe last)
                    self.transmitt_next_chunk()

                    # Disconnect
                    if self.status == ConnectionStatus.DONE:
                        self.sock.close()
                        self.root.drop_connection(self.client_port)
                        self.root.end = True
                        break
                # ------------- Weird Stuff ------------------
                else:
                    raise ValueError

                # Wait for next message
                attempts = 0
                while 1:
                    try:
                        self.data, sender = self.sock.recvfrom(4 * self.BLOCK_SIZE)
                        logging.info("{}: Received {} from {}".format(self.root.uuid, self.data, sender))
                    except socket.timeout:
                        if attempts < 10:
                            attempts += 1
                            continue
                        else:
                            self.status = ConnectionStatus.DONE
                            break
                    if sender == self.client_port:
                        break
            except OSError:
                break
            except Exception as e:
                logging.error("{} : Cannot send response. {}".format(self.root.uuid, e))


class TFTPServer(TFTP):
    def __init__(self, host="127.0.0.1", port=69, path="."):
        os.chdir(path)
        self.TIDs = {}
        self.end = False
        self.TIMEOUT = 0.5
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(self.TIMEOUT)
        super(TFTPServer, self).__init__(host, port)

    def __enter__(self):
        try:
            self.sock.bind((self.host, self.server_port))
            logging.info("{} : Listening on port {}".format(self.uuid, self.server_port))
        except Exception as e:
            logging.error("{} : Error during port binding. {}".format(self.uuid, e))
        return self

    def options_acceptance(self, options, sock=None, host=None):
        options_list = []
        for u, v in options.items():
            options_list += str(u).encode('utf-8'), str(v).encode('utf-8')
        options_with_space = intersperse(options_list, OP__)
        query = [OP_OACK,
                 *options_with_space,
                 OP__]
        if host is None:
            host = (self.host, self.server_port)
        if sock is None:
            sock = self.sock
        self.send(query=query, remote=host, sock=sock)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def new_connection(self, client_port, data):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((self.host, 0))
        self.TIDs[client_port] = Connection(sock, client_port, self, data)
        return sock

    def drop_connection(self, client_port):
        self.TIDs.pop(client_port)
        #self.end = True
        logging.info("{} : Disconnected with host {}".format(self.uuid, client_port))

    def run_forever(self):
        try:
            while not self.end:
                try:
                    data, client_port = self.sock.recvfrom(self.MAX_PACKET_LENGTH)
                    if client_port not in self.TIDs:
                        logging.info("{} : New connection with host {}".format(self.uuid, client_port))
                        self.new_connection(client_port, data)
                        self.TIDs[client_port].start()
                except Exception:
                    pass
        except KeyboardInterrupt:
            self.exit()

    def exit(self, *args, **kwargs):
        for port, conn in self.TIDs.items():
            conn.sock.close()
        exit()


if __name__ == '__main__':
    logging.basicConfig(level=logging.CRITICAL)
    #logging.basicConfig(level=logging.INFO)

    init_connection_vars(sys.argv)
    global host, port, filename

    with TFTPServer("127.0.0.1", port, path) as server:
        server.run_forever()
