#!/usr/bin/python3

import hashlib
import socket
import struct
import uuid
import sys

#  OPCodes
import logging
OP__ = b'\x00'  # Space
OP_RPQ = b'\x00\x01'  # Read request
OP_WRQ = b'\x00\x02'  # Write request
OP_DATA = b'\x00\x03'  # Data
OP_ACK = b'\x00\x04'  # Accepted
OP_Err = b'\x00\x05'   # Error
OP_OACK = b'\x00\x06' # Options Acceptance


def create_message(list_of_bytes):
    return b''.join(list_of_bytes)


def intersperse(lst, item):
    result = [item] * (len(lst) * 2 - 1)
    result[0::2] = lst
    return result


MAX_BLOCK_ID = 2**16 - 1
MAXIMAL_ACCEPTANCE_BLOCKSIZE = 2**13 - 1


def next_block_id(block_id):
    return (block_id + 1) % (MAX_BLOCK_ID + 1)


class TFTP(object):
    def __init__(self, host="127.0.0.1", port=69):
        self.host = host
        self.server_port = port
        self.mode = b"octet"
        self.uuid = uuid.uuid1()
        self.last_message = b''
        self.MAX_NUMBER_OF_CONNECTIONS = 1
        self.MAX_DATA_LENGTH = 512
        self.MAX_PACKET_LENGTH = 4 + self.MAX_DATA_LENGTH

    def send_block_accept(self, block_id, sock=None, host=None):
        #print(block_id)
        query = [OP_ACK,
                 struct.pack('>H', block_id)]
        if host is None:
            host = (self.host, self.server_port)
        if sock is None:
            sock = self.sock
        self.send(query=query, remote=host, sock=sock)

    def send_data_block(self, data, block_id, sock=None, host=None):
        query = [OP_DATA,
                 struct.pack('>H', block_id),
                 data]
        if host is None:
            host = (self.host, self.server_port)
        if sock is None:
            sock = self.sock
        self.send(query=query, remote=host, sock=sock)

    def send(self, query, remote=None, sock=None):
        msg = create_message(query)
        if sock is None:
            sock = self.sock
        if remote is None:
            remote = (self.host, self.server_port)
        logging.info("{} : Sending message {} to {}".format(self.uuid, msg, remote))
        sock.sendto(msg, remote)
        self.last_message = msg

    def run_forever(self):
        raise NotImplementedError

    def exit(self, *args, **kwargs):
        raise NotImplementedError

    def __enter__(self):
        raise NotImplementedError

    def __exit__(self, exc_type, exc_val, exc_tb):
        raise NotImplementedError


def init_connection_vars(args):
    global host, port, filename

    host = "localhost"
    port = 69
    filename = ''

    l = len(args)
    if l > 1:
        host = args[1]
    if l > 2:
        filename = args[2]
    if l > 3:
        port = int(args[2])
        filename = args[3]


class TFTPClient(TFTP):
    def __init__(self, host="127.0.0.1", port=69):
        self.private_port = -1
        self.connected = False
        self.TIMEOUT = 0.1
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(self.TIMEOUT)
        self.PREFERRED_BLOCK_SIZE = 4096
        self.PREFERRED_WINDOW_SIZE = 16
        self.BLOCK_SIZE = 512
        self.WINDOW_SIZE = 1
        self.in_window_pos = 0
        super(TFTPClient, self).__init__(host, port)

    def __enter__(self):
        self.connect(self.host, self.server_port)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.sock.close()
        logging.info("{} : Disconnected".format(self.uuid))

    def prompt(self):
        print("tftp> ", end='')
        line = input()
        parts = line.split(' ')
        return parts[0], parts[1:]

    def execute_command(self, command, args=None):
        commands = {
            'connect': self.connect,
            'exit': self.exit,
            'get': self.get,
            'put': self.put,
        }
        if len(command) == 0:
            return
        if command in commands:
            commands[command](*args)
        else:
            raise ValueError

    def run_forever(self):
        try:
            while 1:
                command, args = self.prompt()
                try:
                    self.execute_command(command, args)
                except Exception:
                    print("Command invalid")
                    logging.warning("{} : Command {} with arguments {} is invalid".format(self.uuid, command, args))
        except KeyboardInterrupt:
            print('')
            self.exit()

    def connect(self, host="127.0.0.1", port=69, *args, **kwargs):
        port = int(port)
        self.sock.close()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(self.TIMEOUT)
        self.host = host
        self.server_port = port
        logging.info("{} : Connected to {}".format(self.uuid, (self.host, self.server_port)))

    def exit(self, *args, **kwargs):
        exit()

    def get(self, filename, *args, **kwargs):
        encoded_filename = filename.encode('utf-8')
        ending = False
        conn = None
        hash = hashlib.new('md5')
        read_request = [OP_RPQ,
                        encoded_filename,
                        OP__,
                        self.mode,
                        OP__,
                        'blksize'.encode('utf-8'),
                        OP__,
                        str(self.PREFERRED_BLOCK_SIZE).encode('utf-8'),
                        OP__,
                        'windowsize'.encode('utf-8'),
                        OP__,
                        str(self.PREFERRED_WINDOW_SIZE).encode('utf-8'),
                        OP__,
                        ]

        self.send(read_request)

        # Reading chunks
        try:
            with open(filename, 'wb') as file:
                block_id = 0
                while 1:
                    timeouts = 0
                    try:
                        msg, conn = self.sock.recvfrom(10 * self.BLOCK_SIZE)
                        logging.info("{} : Received {}".format(self.uuid, msg))
                    except socket.timeout:
                        if timeouts < 1024:
                            timeouts += 1
                            self.send_block_accept(block_id=block_id, host=conn)
                            self.in_window_pos = 0
                            continue
                        else:
                            break
                    # OACK detect
                    command, parts = msg[:2], msg[2:].split(OP__)
                    if command == OP_OACK:
                        # Get options
                        parts = [x.decode('utf-8') for x in parts]
                        options = dict(zip(parts[:-1:2], parts[1::2]))
                        if 'blksize' in options:
                            self.BLOCK_SIZE = int(options['blksize'])
                        if 'windowsize' in options:
                            self.WINDOW_SIZE = int(options['windowsize'])
                        self.send_block_accept(block_id=0, host=conn)
                    else:
                        received_block_id = struct.unpack(">H", msg[2:4])[0]
                        if received_block_id == next_block_id(block_id):
                            block_id = next_block_id(block_id)
                            data = msg[4:]
                            hash.update(data)
                            file.write(data)
                            if len(data) < self.BLOCK_SIZE:
                                ending = True
                            self.in_window_pos += 1
                            if self.in_window_pos >= self.WINDOW_SIZE or ending:
                                self.send_block_accept(block_id=block_id, host=conn)
                                self.in_window_pos = 0
                        else:
                            self.in_window_pos = 0
                            self.send_block_accept(block_id=block_id, host=conn)
                    if ending:
                        break
        except Exception:
            pass

        print(hash.hexdigest())


if __name__ == '__main__':
    logging.basicConfig(level=logging.CRITICAL)
    #logging.basicConfig(level=logging.INFO)

    init_connection_vars(sys.argv)
    global host, port, filename

    with TFTPClient(host, port) as client:
        client.get(filename)
