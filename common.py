import struct
import uuid

#  OPCodes
import logging
OP__ = b'\x00'  # Space
OP_RPQ = b'\x00\x01'  # Read request
OP_WRQ = b'\x00\x02'  # Write request
OP_DATA = b'\x00\x03'  # Data
OP_ACK = b'\x00\x04'  # Accepted
OP_Err = b'\x00\x05'   # Error
OP_OACK = b'\x00\x06' # Options Acceptance


def create_message(list_of_bytes):
    return b''.join(list_of_bytes)


MAX_BLOCK_ID = 2**16 - 1


def next_block_id(block_id):
    return (block_id + 1) % (MAX_BLOCK_ID + 1)


def intersperse(lst, item):
    result = [item] * (len(lst) * 2 - 1)
    result[0::2] = lst
    return result


class TFTP(object):
    def __init__(self, host="127.0.0.1", port=69):
        self.host = host
        self.server_port = port
        self.mode = b"octet"
        self.uuid = uuid.uuid1()
        self.last_message = b''
        self.MAX_NUMBER_OF_CONNECTIONS = 1
        self.MAX_DATA_LENGTH = 512
        self.MAX_PACKET_LENGTH = 4 + self.MAX_DATA_LENGTH
        self.MAX_BLOCK_ID = 65535

    def send_block_accept(self, block_id, sock=None, host=None):
        query = [OP_ACK,
                 struct.pack('>H', block_id)]
        if host is None:
            host = (self.host, self.server_port)
        if sock is None:
            sock = self.sock
        self.send(query=query, remote=host, sock=sock)

    def send_data_block(self, data, block_id, sock=None, host=None):
        query = [OP_DATA,
                 struct.pack('>H', block_id),
                 data]
        if host is None:
            host = (self.host, self.server_port)
        if sock is None:
            sock = self.sock
        self.send(query=query, remote=host, sock=sock)

    def send(self, query, remote=None, sock=None):
        msg = create_message(query)
        if sock is None:
            sock = self.sock
        if remote is None:
            remote = (self.host, self.server_port)
        logging.info("{} : Sending message {} to {}".format(self.uuid, msg, remote))
        sock.sendto(msg, remote)
        self.last_message = msg

    def next_block_id(self, block_id):
        return (block_id + 1) % (self.MAX_BLOCK_ID + 1)

    def options_acceptance(self, options, sock=None, host=None):
        options_list = []
        for u, v in options.items():
            options_list += str(u).encode('utf-8'), str(v).encode('utf-8')
        options_with_space = intersperse(options_list, OP__)
        query = [OP_OACK,
                 *options_with_space,
                 OP__]
        if host is None:
            host = (self.host, self.server_port)
        if sock is None:
            sock = self.sock
        self.send(query=query, remote=host, sock=sock)

    def run_forever(self):
        raise NotImplementedError

    def exit(self, *args, **kwargs):
        raise NotImplementedError

    def __enter__(self):
        raise NotImplementedError

    def __exit__(self, exc_type, exc_val, exc_tb):
        raise NotImplementedError