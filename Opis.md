# Trivial File Transfer Protocol

---

Implementacja serwera i klienta dla protokołu TFTP w języku Python.

__Rafał Jankowski__

Klient i serwer przechodzą testy na satori (w wersji dla domyślnego windowsize i blksize) i dla (16 windowsize, 4096 blksize). Testowane było przesyłanie plików o rozmiarach >1GB, wielokrotność blksize i 0 bajtów. Serwer był sprawdzany przy użyciu standardowego klienta tftp (atftp) i z wbudowanego dla MacOS serwera. Serwer obsługuje wielowątkowe trzymanie zapytań co pozwala na jednoczesną komunikację z wieloma użytkownikami.

Projekt składa się z:

1. __client.py__ - Implementacja klienta. Uruchamiana tak jak na Satori. Atrybuty `PREFERRED_BLOCK_SIZE` i `PREFFERED_WINDOW_SIZE` są używane do negocjacji z serwerem rozmiaru bloku i okienka.
2. __server.py__ - Implementacja serwera "jednorazowego" zgodna z Satori
3. __serve_forever.py__ - Implementacja demona zdolnego do wielu komunikacji jednocześnie. Różnica pomiędzy __server.py__ polega na wykomentowaniu 2-3 linijek.